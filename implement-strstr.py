class Solution:
    def strStr(self, haystack: str, needle: str) -> int:

        """

        :param haystack:
        :param needle:
        :return:
        """

        if not needle:
            return 0
        if len(haystack) < len(needle):
            return -1
        if not haystack:
            return 0
        for i in range(0,len(haystack)):
            if haystack[i] == needle[0]:
                temp = haystack[i: (i+len(needle))]
                if needle == temp:
                    return i

        return -1

myObj = Solution()
print("index:",myObj.strStr("w er "," er "))