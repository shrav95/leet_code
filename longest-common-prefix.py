class Solution:
    def longestCommonPrefix(self, strs: List[str]) -> str:
        if len(strs) == 0:
            return ""
        else:
            shortestString = strs[0]
        counter = 0
        for string in strs:
            shortestString = string if len(string)<len(shortestString) else shortestString
        #At this point we have the Shortest String in the code
        while(len(shortestString)>0):
            for string in strs:
                if string.startswith(shortestString):
                    counter = counter + 1
                else:
                    shortestString = shortestString[:-1]
                    break
            if counter == len(strs):
                return shortestString
            counter = 0
        return ""
