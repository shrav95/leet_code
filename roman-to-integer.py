from typing import List

class Solution:
    def romanToInt(self, s: str) -> int:
        """

        :param s:
        :return:
        """
        singles = dict(I = 1, V = 5, X = 10, L = 50, C = 100, D = 500, M = 1000)
        doubles = dict(IV = 4, IX = 9, XL = 40, XC = 90, CD = 400, CM = 900)
        integer = 0
        ch = 0
        while(ch != len(s)):
            if ch != len(s) - 1:
                numS = s[ch] + s[ch + 1]
                if numS in doubles:
                    integer = integer + doubles.get(numS)
                    ch = ch + 2
                else:
                    integer = integer + singles.get(s[ch])
                    ch = ch + 1
            else:
                integer = integer + singles.get(s[ch])
                ch = ch + 1

        return integer


myObject = Solution()
print(myObject.romanToInt("XIV"))