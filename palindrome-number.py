import sys
import random
class Solution:
    def isPalindrome(self, x: int) -> bool:
        if -sys.maxsize-1 <= x <= sys.maxsize:
            if x < 0:
                return False
            else:
                reversex = 0
                num = x
                while True:
                    reversex = reversex + num%10
                    num = (num/10).__int__()
                    if num == 0:
                        break
                    reversex = reversex * 10
                if reversex == x:
                    return True
                return False

myObject = Solution()
print(myObject.isPalindrome(10))
# for i in range(10):
#     param = random.randint(-sys.maxsize-1, sys.maxsize)
#     result = myObject.isPalindrome(param)
#     print(param)
#     print(result)
