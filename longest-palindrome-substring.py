class Solution:

    def longestPalindrome(self, s: str) -> str:
        """
        type s : str
        rtype: str

        """
        listOfPalindromes = []
        lengthOfString = len(s)
        testStr = s
        longestPalindrome = ""
        if len(s) == 0:
            return ""
        elif len(s) == 1:
            return s
        elif(len(s) <= 1000):
            for i in range(0, len(testStr)):
                while (lengthOfString != i):
                    s = testStr[i: lengthOfString]
                    revS = s[::-1]
                    if (s == revS):
                        listOfPalindromes.append(revS)
                    lengthOfString = lengthOfString-1
                lengthOfString = len(testStr)

            maxLengthOfS = -1000
            for palindrome in listOfPalindromes:
                if len(palindrome) > maxLengthOfS:
                    maxLengthOfS = len(palindrome)
                    longestPalindrome = palindrome

        return longestPalindrome


mySolution = Solution()
print(mySolution.longestPalindrome("civilwartestingwhetherthatnaptionoranynartionsoconceivedandsodedicatedcanlongendureWeareqmetonagreatbattlefiemldoftzhatwarWehavecometodedicpateaportionofthatfieldasafinalrestingplaceforthosewhoheregavetheirlivesthatthatnationmightliveItisaltogetherfangandproperthatweshoulddothisButinalargersensewecannotdedicatewecannotconsecratewecannothallowthisgroundThebravelmenlivinganddeadwhostruggledherehaveconsecrateditfaraboveourpoorponwertoaddordetractTgheworldadswfilllittlenotlenorlongrememberwhatwesayherebutitcanneverforgetwhattheydidhereItisforusthelivingrathertobededicatedheretotheulnfinishedworkwhichtheywhofoughtherehavethusfarsonoblyadvancedItisratherforustobeherededicatedtothegreattdafskremainingbeforeusthatfromthesehonoreddeadwetakeincreaseddevotiontothatcauseforwhichtheygavethelastpfullmeasureofdevotionthatweherehighlyresolvethatthesedeadshallnothavediedinvainthatthisnationunsderGodshallhaveanewbirthoffreedomandthatgovernmentofthepeoplebythepeopleforthepeopleshallnotperishfromtheearth"))