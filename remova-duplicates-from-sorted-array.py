from typing import List

class Solution:

    numsList = []
    def removeDuplicates(self, nums: List[int]) -> int:
        """

        :param nums:
        :return:
        """
        if len(nums) > 0:
            pos = 0

            for i in range(0, len(nums)):
                if nums[i] != nums[pos]:
                    pos = pos+1
                    nums[pos] = nums[i]

            return (pos+1)

        return 0


    def printList(self, nums: List[int]):
        print(nums)


myObj = Solution()
myObj.numsList = [0,0,1,1,1,2,2,3,3,4,5]
myObj.printList(myObj.numsList)
len = myObj.removeDuplicates(myObj.numsList)
print("len :" , len)
myObj.printList(myObj.numsList)

