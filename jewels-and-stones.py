class Solution:
    def numJewelsInStones(self, J: str, S: str) -> int:
        jewels = dict()
        sum = 0
        for jewel in J:
            jewels.update({jewel:0})
        for stone in S:
            if stone in jewels:
                jewels[stone] = jewels[stone] + 1
        for k ,v in jewels.items():
            sum = sum + v
        return sum