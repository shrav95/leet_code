class Solution:
    def intToRoman(self, num: int) -> str:
        romanEquivalentForInt = {1: 'I', 5: 'V', 10: 'X', 50: 'L', 100: 'C', 500: 'D', 1000: 'M'}
        digitPositionMap = {1: 'I', 2: 'X', 3: 'C'}
        romanEqString = ""
        digitPosition = 0
        if (1 <= num <= 3999):
            while (num != 0):
                remainder = num % 10
                num = num / 10
                num = int(num)
                digitPosition = digitPosition+1
                if (remainder < 4):
                    romanEqString = self.getLessThan4(int(remainder), digitPosition)+romanEqString
                elif (remainder == 5):
                    return romanEquivalentForInt[5 * (10 ^ (digitPosition-1))]+romanEqString
                elif (4 < remainder < 9):
                    romanEqString = self.getGreaterThan4andLessThan9(int(remainder), digitPosition)+romanEqString
                else:
                    romanEqString = self.get4or9(int(remainder), digitPosition)+romanEqString

        return romanEqString

    def getLessThan4(self, num: int, digitPosition: int) -> str:
        digitPositionMap = {1: 'I', 2: 'X', 3: 'C', 4: 'M'}
        romanString = ""
        for i in range(0, num):
            romanString = romanString+digitPositionMap[digitPosition]
        return romanString

    def getGreaterThan4andLessThan9(self, num: int, digitPosition: int) -> str:
        romanString = ""
        romanEquivalentForInt = {1: 'I', 5: 'V', 10: 'X', 50: 'L', 100: 'C', 500: 'D', 1000: 'M'}
        digitPositionMap = {1: 'I', 2: 'X', 3: 'C'}
        romanString = romanString+romanEquivalentForInt[5 * (10 ^ (digitPosition-1))]
        for i in range(0, num):
            romanString = romanString+digitPositionMap[digitPosition]

        return romanString

    def get4or9(self, num: int, digitPosition: int) -> str:
        romanString = ""
        romanEquivalentForInt = {1: 'I', 5: 'V', 10: 'X', 50: 'L', 100: 'C', 500: 'D', 1000: 'M'}
        digitPositionMap = {1: 'I', 2: 'X', 3: 'C'}
        return romanString+digitPositionMap[digitPosition]+romanEquivalentForInt[
            (num * (10 ^ (digitPosition-1)))+(10 ^ (digitPosition-1))]

myobjectx = Solution()
print(myobjectx.intToRoman(58))
