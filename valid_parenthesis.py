class ValidParenthesisSolution:
    def isValid(self, s: str) -> bool:
        """

        :param s:
        :return:
        """
        if s == "":
            return True
        count = 1
        correspondParen = {'(': ')' , "{" :  "}" , "[" : "]"}
        resultList =[s[0]]

        for index in range(1, len(s)):
            if count > 0 and s[index] == correspondParen.get(resultList[(count -1)]):
                resultList.pop(count - 1);
                count -= 1
            else:
                resultList.append(s[index])
                count += 1

        if count == 0:
            return True
        else:
            return False

myObject = ValidParenthesisSolution()
print("Result: ", myObject.isValid(""))