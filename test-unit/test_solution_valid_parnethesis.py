import unittest
from unittest import TestCase
from parameterized import parameterized
from valid_parenthesis import ValidParenthesisSolution


class TestSolutionValidParenthesis(TestCase):

    @parameterized.expand([
        ("OpenAndCloseImmediately", "(){}[]", True),
        ("AllOpenThenAllClose", "{([])}", True),
        ("SomeOpenWithinOthers", "[{}]([]){()}", True),
        ("SomeOpenDontClose", "{(]}", False),
        ("AllOpenDontClose", "({[", False),
        ("AllClose", "})]", False)
    ])
    def test_isValid(self, name, input, output):
        self.assertEqual(ValidParenthesisSolution().isValid(input), output)

    if __name__ == '__main__':
        unittest.main()
