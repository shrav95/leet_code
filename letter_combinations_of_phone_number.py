from typing import List

class Solution:
    def letterCombinations(self, digits: str) -> List[str]:

        letter_dict = {
            2:'abc',
            3:'def',
            4:'ghi',
            5:'jkl',
            6:'mno',
            7:'pqrs',
            8:'tuv',
            9:'wxyz'
        }
        if digits != '':
            digit_list = list(digits)
        else:
            return [""]


        # print(digit_list)
        res = list(letter_dict.get(int(digit_list[0])))
        # print(res)

        for i in range(1, len(digit_list)):
            res = [x+y for x in res for y in list(letter_dict.get(int(digit_list[i])))]

        return res


object_solution = Solution()
print(object_solution.letterCombinations(""))
