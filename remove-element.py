from typing import List

class Solution:
    def removeElement(self, nums: List[int], val: int) -> int:
        """

        :param nums:
        :param val:
        :return:
        """
        if len(nums) == 0:
            return 0

        pos = 0
        for j in range(0,len(nums)):
            if nums[j] != val:
                nums[pos] = nums[j]
                pos = pos + 1

        return pos


myObject = Solution()
myObject.myList = [2,2,4,5,3,6,2]
print("before:",myObject.myList)
print("len:",myObject.removeElement(myObject.myList, 2))
print("after:", myObject.myList)


# if not nums:
#             return 0
#         index=0
#         length= len(nums)
#         while index < length:
#             if nums[index] ==val:
#                 del nums[index]
#                 index -= 1
#                 length -= 1
#             index += 1
#         return index